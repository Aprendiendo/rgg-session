//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/json/jsonCoreExportParameters.h"

namespace smtk
{
namespace session
{
namespace rgg
{
using json = nlohmann::json;

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const CoreExportParameters& aep)
{
  j["geometry"] = aep.Geometry;
  j["geometryType"] = aep.GeometryType;
  j["geomEngine"] = aep.GeomEngine;
  j["extrude"] = aep.Extrude;
  if (aep.MergeTolerance >= 0)
  {
    j["mergeTolerance"] = aep.MergeTolerance;
  }
  j["neumannSet"] = aep.NeumannSet;
  j["outputFileName"] = aep.OutputFileName;
  j["backgroundInfo"] = aep.BackgroundInfo;
  j["problemType"] = aep.ProblemType;
  j["saveParallel"] = aep.SaveParallel;
  j["info"] = aep.Info;
  j["meshINFO"] = aep.MeshINFO;
  j["symmetry"] = aep.Symmetry;
}

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, CoreExportParameters& aep)
{
  try
  {
    aep.Geometry = j.at("geometry");
    aep.GeometryType = j.at("geometryType"); // Hexagonal or Rectangular
    aep.GeomEngine = j.at("geomEngine");
    aep.Extrude = j.at("extrude");
    if (j.find("mergeTolerance") != j.end())
    {
      aep.MergeTolerance = j.at("mergeTolerance");
    }
    json neumannSetJson = j.at("neumannSet");
    for (json::iterator it = neumannSetJson.begin(); it != neumannSetJson.end();++it)
    {
      aep.NeumannSet.push_back(*it);
    }
    aep.OutputFileName = j.at("outputFileName");
    aep.BackgroundInfo = j.at("backgroundInfo");
    aep.ProblemType = j.at("problemType");
    aep.SaveParallel = j.at("saveParallel");
    aep.Info = j.at("info");
    aep.MeshINFO = j.at("meshINFO");
    aep.Symmetry = j.at("symmetry");
  }
  catch
  (json::exception& e)
  {
    std::cerr << e.what() <<std::endl;
    std::cerr << "Failed to parse the CoreExportParameter in the core" <<std::endl;
  }
}

} // namespace rgg
} // namespace session
} // namespace smtk

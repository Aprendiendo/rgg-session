<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "EditPin" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="edit pin" Label="Model - Edit Pin" BaseType="operation">
      <BriefDescription>Edit a RGG Pin</BriefDescription>
      <DetailedDescription>
        If the association is a model, then it's in creation mode. The pin may
        consist of several cylinders and frustums. It can also have several
        materals defined from the out layer to inner layer.
        If the association is an existing RGG pin, it's in editing mode. the
        selected pin would be used to populate the panel.
      </DetailedDescription>
      <AssociationsDef Name="model/pin" NumberOfRequiredValues="1" AdvanceLevel="0">
        <MembershipMask>aux_geom | model </MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="pin representation" NumberOfRequiredValues="1" AdvanceLevel="11">
          <BriefDescription>A json representation for the nuclear pin</BriefDescription>
          <DetailedDescription>
            A json representation for the modified nuclear pin. See details in jsonPin.h.
          </DetailedDescription>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit pin)" BaseType="result">
      <ItemDefinitions>
        <!-- The edited pin is returned in the base result's "edit" item. -->
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
     <!--
      The customized view "Type" needs to match the plugin's VIEW_NAME:
      add_smtk_ui_view(...  VIEW_NAME smtkRGGEditPinView ...)
      -->
    <View Type="smtkRGGEditPinView" Title="Edit Pin"  FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="false">
      <Description>
        A view for changing a nuclear pin's properties.
      </Description>
      <AttributeTypes>
        <Att Type="edit pin"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>

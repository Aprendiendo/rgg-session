#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

""" export_to_pyarc.py:

Export an RGG model as PyARC geometry.

"""
import sys

from . import export_to_pyarc_xml

import os
import rggsession
import smtk
import smtk.io
import smtk.model
from .export_to_pyarcHelper import *


class export_to_pyarc(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "export to pyarc"

    def operateInternal(self):

        # Access the PyARC SON file
        filename = self.parameters().findFile('filename').value(0)

        # Access the associated model
        model = smtk.model.Entity.CastTo(self.parameters().associations().objectValue(0))
        modelRef = smtk.model.Model(model)
        isHex = modelRef.integerProperty("hex")[0]
        if not isHex:
            smtk.InfoMessage(
                self.log(), 'Cannot export a non hex nuclear core!')
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Awesome print message
        smtk.InfoMessage(self.log(), 'Executing export_to_pyarc')

        materials = modelRef.stringProperty('materials')
        materialDescriptions = modelRef.stringProperty('material_descriptions')
        # TODO: export materials

        tabNum = 0

        surfaces = dict()
        # (name, orientation, normal, pitch)
        surfaces["hexagon"] = set()
        # (name, axis, radius)
        surfaces["cylinder"] = set()
        # (name, z)
        surfaces["plane"] = set()

        content = "=arc\ngeometry{\n"
        materialsS, surfacesS, coreS, calculationsS = "", "", "", ""
        for group in modelRef.groups():
            rggType = group.stringProperty('rggType')[0]
            if (rggType == '_rgg_core'):
                smtk.InfoMessage(self.log(), "processing core " + group.name())
                core = Core(group)
                coreS = core.exportCore(tabNum + 1, surfaces)
                break
        surfacesS += surfacesToString(tabNum + 1, surfaces)

        content += materialsS
        content += surfacesS
        content += coreS
        content += calculationsS
        content += "}"

        writeFile(filename, content)

        # Return with success
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        return result

    def createSpecification(self):
        spec = smtk.attribute.Resource.create()
        reader = smtk.io.AttributeReader()
        reader.readContents(
            spec, export_to_pyarc_xml.description, self.log())
        return spec

<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "EditDuct" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="edit duct" Label="Model - Edit Duct" BaseType="operation">
      <BriefDescription>Edit a RGG Duct.</BriefDescription>
      <DetailedDescription>
        If the assocation is a model, then it's in creation mode.
        Users can edit pre defined duct properties.
        If the association is an existing RGG duct, it's in editing mode. The
        selected duct would be used to populate the panel.
        Its pitch and height are pre-defined in the core.
      </DetailedDescription>
      <AssociationsDef Name="model/duct" NumberOfRequiredValues="1" AdvanceLevel="0">
        <MembershipMask>aux_geom | model </MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="duct representation" NumberOfRequiredValues="1" AdvanceLevel="11">
          <BriefDescription>A json representation for the nuclear duct</BriefDescription>
          <DetailedDescription>
            A json representation for the nuclear duct. See details in jsonDuct.h.
          </DetailedDescription>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit duct)" BaseType="result">
      <ItemDefinitions>
        <!-- The edit duct is returned in the base result's "edit" item. -->
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
     <!--
      The customized view "Type" needs to match the plugin's VIEW_NAME:
      add_smtk_ui_view(...  VIEW_NAME smtkRGGEditDuctView ...)
      -->
    <View Type="smtkRGGEditDuctView" Title="Edit Duct"  FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="false">
      <Description>
        TODO: Add documentation for edit duct operator.
      </Description>
      <AttributeTypes>
        <Att Type="edit duct"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>

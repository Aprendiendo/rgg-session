//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/plugin/smtkRGGEditCoreView.h"
#include "smtk/session/rgg/plugin/ui_smtkRGGEditCoreParameters.h"

#include "smtkRGGViewHelper.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"

#include "smtk/io/Logger.h"

#include "smtk/model/AuxiliaryGeometry.h"
#include "smtk/model/Group.h"
#include "smtk/model/Resource.h"

#include "smtk/operation/Operation.h"

#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/Core.h"

#include "smtk/session/rgg/json/jsonAssembly.h"
#include "smtk/session/rgg/json/jsonCore.h"

#include "smtk/session/rgg/operators/CreateModel.h"
#include "smtk/session/rgg/operators/EditCore.h"

#include "smtk/session/rgg/qt/qtDraw2DLattice.h"
#include "smtk/session/rgg/qt/rggNucCore.h"

#include "smtk/extension/qt/qtActiveObjects.h"
#include "smtk/extension/qt/qtAttribute.h"
#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/qtItem.h"
#include "smtk/extension/qt/qtModelOperationWidget.h"
#include "smtk/extension/qt/qtModelView.h"
#include "smtk/extension/qt/qtUIManager.h"

#include "smtk/view/View.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqPresetDialog.h"

#include <QComboBox>
#include <QMap>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QVBoxLayout>

#include <QDockWidget>

using namespace smtk::model;
using namespace smtk::extension;
using namespace smtk::session::rgg;
using json = nlohmann::json;

static constexpr double cos30 = 0.86602540378443864676372317075294;
static constexpr double cos60 = 0.5;
// static const int degreesHex[6] = { -120, -60, 0, 60, 120, 180 };
// static const int degreesRec[4] = { -90, 0, 90, 180 };
// 0', 60', 120, 180', 240', 300'
static const double cosSinAngles[6][2] = { { 1.0, 0.0 }, { cos60, -cos30 }, { -cos60, -cos30 },
  { -1.0, 0.0 }, { -cos60, cos30 }, { cos60, cos30 } };

namespace
{

// Calculate the x,y coordinates of the current assy in the hex grid
std::pair<double, double> calculateHexAssyCoordinate(const double& spacing, const int& ring, const int& layer)
{
  double x, y;
  // The index order of layer is clockwise, starting from upper left corner of the hex.
  // It's RGG's order and not ideal...
  if (ring == 0)
  {
    x = y = 0;
  }
  else
  {
    int remainder = layer % ring;
    int modulus = layer / ring;

    double eL = ring * spacing;                       // edge length
    double xBT = -eL * cos60 + eL / (ring)*remainder; // x before transform
    double yBT = eL * cos30;                          // y befor transform
    // Apply rotation if needed. In order to avoid sin/cos calculation, we use
    // predefined values;
    double cosValue = cosSinAngles[modulus][0];
    double sinValue = cosSinAngles[modulus][1];

    double x0 = xBT * cosValue - yBT * sinValue;
    double y0 = yBT * cosValue + xBT * sinValue;

    // Rotate 330 degree due to the fact that the orientations do not match in
    // the render view and schema planner
    // sin330 = -cos60 and cos330 = cos30;
    x = x0 * cos30 - y0 * (-cos60);
    y = y0 * cos30 + x0 * (-cos60);
  }
  return std::make_pair(x, y);
}
}

class smtkRGGEditCoreViewInternals : public Ui::RGGEditCoreParameters
{
public:
  smtkRGGEditCoreViewInternals()
  {
  }

  ~smtkRGGEditCoreViewInternals()
  {
    if (CurrentAtt)
    {
      delete CurrentAtt;
      CurrentAtt = nullptr;
    }
    auto mapIter = this->SMTKCoreToCMBCore.begin();
    while (mapIter != this->SMTKCoreToCMBCore.end())
    {
      if (mapIter.value())
      {
        delete mapIter.value();
      }
      mapIter++;
    }
    if (this->SchemaPlanner)
    {
      this->SchemaPlanner->setVisible(false);
    }
  }

  qtAttribute* createAttUI(smtk::attribute::AttributePtr att, QWidget* pw, qtBaseView* view)
  {
    if (att && att->numberOfItems() > 0)
    {
      smtk::view::View::Component comp;
      qtAttribute* attInstance = new qtAttribute(att, comp, pw, view);
      if (attInstance && attInstance->widget())
      {
        //Without any additional info lets use a basic layout with model associations
        // if any exists
        attInstance->createBasicLayout(true);
        attInstance->widget()->setObjectName("RGGCoreEditor");
        QVBoxLayout* parentlayout = static_cast<QVBoxLayout*>(pw->layout());
        parentlayout->insertWidget(0, attInstance->widget());
      }
      return attInstance;
    }
    return nullptr;
  }

  QPointer<qtAttribute> CurrentAtt{nullptr};
  QPointer<qtDraw2DLattice> Current2DLattice{nullptr};
  QPointer<QDockWidget> SchemaPlanner;
  // Map smtk core(smtk::model::Model) to CMB core rggNucCore which is used for schema planning
  QMap<smtk::model::EntityRef, rggNucCore*> SMTKCoreToCMBCore;
  rggNucCore* CurrentCMBCore{nullptr};
  QPointer<QWidget> CurrentWidget;
  smtk::model::EntityRef CurrentSMTKCore;

  smtk::weak_ptr<smtk::operation::Operation> CreateInstanceOp;
  smtk::weak_ptr<smtk::operation::Operation> DeleteOp;
  smtk::shared_ptr<smtk::operation::Operation> CurrentOp;
};

smtkRGGEditCoreView::smtkRGGEditCoreView(const OperationViewInfo& info)
  : qtBaseView(info)
{
  this->Internals = new smtkRGGEditCoreViewInternals();
  this->Internals->CurrentOp = info.m_operator;
}

smtkRGGEditCoreView::~smtkRGGEditCoreView()
{
  delete this->Internals;
}

qtBaseView* smtkRGGEditCoreView::createViewWidget(const ViewInfo& info)
{
  const OperationViewInfo* opInfo = dynamic_cast<const OperationViewInfo*>(&info);
  if (!opInfo)
  {
    return nullptr;
  }
  smtkRGGEditCoreView* view = new smtkRGGEditCoreView(*opInfo);
  view->buildUI();
  return view;
}

bool smtkRGGEditCoreView::displayItem(smtk::attribute::ItemPtr item)
{
  return this->qtBaseView::displayItem(item);
}

void smtkRGGEditCoreView::requestModelEntityAssociation()
{
  this->updateAttributeData();
}

void smtkRGGEditCoreView::valueChanged(smtk::attribute::ItemPtr /*optype*/)
{
  this->requestOperation(this->Internals->CurrentOp);
}

void smtkRGGEditCoreView::requestOperation(const smtk::operation::OperationPtr& op)
{
  if (!op || !op->parameters())
  {
    return;
  }
  op->operate();
}

void smtkRGGEditCoreView::attributeModified()
{
  // Always enable apply button here
}

void smtkRGGEditCoreView::onAttItemModified(smtk::extension::qtItem* item)
{
  smtk::attribute::ItemPtr itemPtr = item->item();
  // only changing core would update the edit core panel
  if ((itemPtr->name() == "Core") &&
    itemPtr->type() == smtk::attribute::Item::Type::ReferenceType)
  {
    this->updateEditCorePanel();
  }
}

void smtkRGGEditCoreView::apply()
{
  // Due to the limitation of PV 3DGlyphMapper that it cannot create instances
  // based on an instance, I choose to decompose an assembly to pins and a duct.
  // In other words, a rgg core also consists of pin instances and duct instances.

  // Extract some assembly information first
  smtk::attribute::AttributePtr ecAtt = this->Internals->CurrentOp->parameters();

  smtk::model::EntityRefArray coreArray =
    ecAtt->associatedModelEntities<smtk::model::EntityRefArray>();
  smtk::model::Group coreGroup;
  smtk::model::Model model;
  if (coreArray.size() > 0)
  {
    coreGroup = coreArray[0].as<smtk::model::Group>();
    model = coreGroup.owningModel();
  }
  else
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "An invalid core is provided to the op. Stop"
                                                 "the operation.");
    return;
  }

  ResourcePtr resource = model.resource();
  Core core = json::parse(coreGroup.stringProperty(Core::propDescription)[0]);
  bool isHex = (core.geomType() == Core::GeomType::Hex);

  core.setName(this->Internals->nameLineEdit->text().toStdString());

  core.setHeight(this->Internals->heightLineEdit->text().toDouble());

  core.setZOrigin(this->Internals->zOriginLineEdit->text().toDouble());

  if (isHex)
  {
    core.setDuctThickness(this->Internals->ductXThicknessSpinBox->value());
  }
  else
  {
    core.setDuctThickness(this->Internals->ductXThicknessSpinBox->value(),
            this->Internals->ductYThicknessSpinBox->value());
  }

  // Ignore geometry type item since it can only be decided at
  // the creation time
  smtk::attribute::IntItemPtr latticeSizeI = ecAtt->findInt("lattice size");
  std::pair<int, int> latticeSize;
  latticeSize.first = this->Internals->latticeXSpinBox->value();
  if (isHex)
  { // Geometry type is hex.
    latticeSize.second = this->Internals->latticeXSpinBox->value();
  }
  else
  { // Geometry type is rect
    latticeSize.second = this->Internals->latticeYSpinBox->value();
  }
  core.setLatticeSize(latticeSize.first, latticeSize.second);

  // Assembly and its corresponding layouts
  Core::UuidToSchema assyToLayout;
  // Call apply function on qtDraw2DLattice to update CurrentCMBCore
  if (this->Internals->Current2DLattice)
  {
    this->Internals->Current2DLattice->apply();
  }

  // FIXME: This map might be a performance bottleneck if we are considering
  // millions of assemblies
  std::shared_ptr<qtLattice> coreLattice = this->Internals->CurrentCMBCore->lattice();
  std::pair<size_t, size_t> dimension = coreLattice->GetDimensions();
  for (size_t i = 0; i < dimension.first; i++)
  {
    size_t jMax = isHex ? ((i == 0) ? 1 : dimension.second * i) : dimension.second;
    for (size_t j = 0; j < jMax; j++)
    {
      smtk::model::EntityRef assyEntity = coreLattice->GetCell(i, j).getPart();
      if (!assyEntity.hasStringProperty(Assembly::propDescription))
      {
        continue;
      }
      Assembly assy = json::parse(assyEntity.stringProperty(Assembly::propDescription)[0]);
      // Skip invalid and empty assy cell(defined in qtLattice class)
      if (!assyEntity.isValid() ||
        (assy.name() == "Empty Cell" && assy.label() == "XX"))
      {
        continue;
      }

      if (assyToLayout.find(assyEntity.entity()) != assyToLayout.end())
      {
        assyToLayout[assyEntity.entity()].push_back(std::make_pair(i,j));
      }
      else
      {
        std::vector<std::pair<int, int>> layout;
        layout.push_back(std::make_pair(i,j));
        assyToLayout.emplace(assyEntity.entity(), layout);
      }
    }
  }
  core.setLayout(assyToLayout);

  // Pins and ducts
  // Calculate the starting point first
  std::pair<double, double> spacing = core.ductThickness();
  double baseX, baseY;
  if (!isHex)
  { // Use the cartesian coordinate where the starting point is located
    // at left bottom
    baseX = -1 * spacing.first * (static_cast<double>(latticeSize.first) / 2 - 0.5);
    baseY = -1 * spacing.second * (static_cast<double>(latticeSize.second) / 2 - 0.5);
  }
  else
  { // Spacing is the allowable max distance between two adjacent assembly centers
    // Use the cartesian coordinate where the starting point is located at
    // the origin point.
    baseX = baseY = 0.0; // Ignored by calculateHexAssyCoordinate for now
  }

  // Map pins&ducts to their placements
  Core::UuidToCoordinates pdToCoords, assyToCoords;
  // Loop through each assembly, calculate the new global coords for its duct and pins
  for (auto iter = assyToLayout.begin(); iter != assyToLayout.end(); iter++)
  { // For each assembly, retrieve its pins&duct info, apply the right transformation
    // then add it into pinDuctToLayout map

    smtk::model::EntityRef assyEntity = smtk::model::EntityRef(resource, iter->first);
    if (!assyEntity.hasStringProperty(Assembly::propDescription))
    {
      smtkErrorMacro(smtk::io::Logger::instance(), "An invalid assembly is provided"
                                                   " in the core layout");
    }
    const std::vector<std::pair<int, int>>& assyLayout = iter->second;
    Assembly assy = json::parse(assyEntity.stringProperty(Assembly::propDescription)[0]);
    smtk::common::UUID ductUUID = assy.associatedDuct();

    auto pinsToLocalCoords = assy.entityToCoordinates();

    std::vector<std::tuple<double, double, double>> assyCoordinates;
    for (size_t index = 0; index < assyLayout.size(); index++)
    {
      double x, y;
      if (isHex)
      {
        auto xAndy = calculateHexAssyCoordinate(spacing.first, assyLayout[index].first, assyLayout[index].second);
        x = xAndy.first;
        y = xAndy.second;
      }
      else
      {
        // In schema planner, x and y axis are following Qt's pattern.
        // Here we just follow the traditional coordinate convension
        x = baseX + spacing.first * assyLayout[index].first;
        y = baseY + spacing.second * assyLayout[index].second;
      }
      assyCoordinates.emplace_back(std::make_tuple(x, y, 0));
      // For each (x,y) pair, add it to every pin and duct in the current assy
      auto transformCoordsAndAddToMap = [&pdToCoords, &x, &y](
        const smtk::common::UUID& ent, std::vector<std::tuple<double, double, double>>& coords) {
        // Apply transformation
        for (size_t i = 0; i < coords.size(); i++)
        {
          // X
          std::get<0>(coords[i]) += x;
          // Y
          std::get<1>(coords[i]) += y;
        }

        if (pdToCoords.find(ent) != pdToCoords.end())
        { // TODO: Possible performance bottleneck
          pdToCoords[ent].insert(pdToCoords[ent].end(), coords.begin(), coords.end());
        }
        else
        {
          pdToCoords[ent] = coords;
        }
      };
      // Duct
      std::vector<std::tuple<double, double, double>> ductCoords = {{ 0, 0, 0 }};
      transformCoordsAndAddToMap(ductUUID, ductCoords);
      // Pins
      for (const auto& iter : pinsToLocalCoords)
      { // Make an explict copy
        std::vector<std::tuple<double, double, double>> coords = iter.second;
        transformCoordsAndAddToMap(iter.first, coords);
      }
    }
    assyToCoords.emplace(iter->first, assyCoordinates);
  }
  core.setEntityToCoordinates(assyToCoords);
  core.setPinsAndDuctsToCoordinates(pdToCoords);
  json coreJson = core;
  std::string coreStr = coreJson.dump();
  ecAtt->findString("core representation")->setValue(coreStr);

  this->requestOperation(this->Internals->CurrentOp);
}

void smtkRGGEditCoreView::launchSchemaPlanner()
{
  if (!this->Internals->SchemaPlanner)
  {
    QWidget* dockP = nullptr;
    foreach (QWidget* widget, QApplication::topLevelWidgets())
    {
      if (widget->inherits("QMainWindow"))
      {
        dockP = widget;
        break;
      }
    }
    this->Internals->SchemaPlanner = new QDockWidget(dockP);
    this->Internals->SchemaPlanner->setObjectName("rggCoreSchemaPlanner");
    this->Internals->SchemaPlanner->setWindowTitle("Core Schema Planner");
    auto sp = this->Internals->SchemaPlanner;
    sp->setFloating(true);
    sp->setWidget(this->Internals->Current2DLattice);
    sp->raise();
    sp->show();
    this->Internals->Current2DLattice->rebuild();
  }
  else
  {
    this->Internals->SchemaPlanner->raise();
    this->Internals->SchemaPlanner->show();
  }
}

void smtkRGGEditCoreView::updateAttributeData()
{
  smtk::view::ViewPtr view = this->getObject();
  if (!view || !this->Widget)
  {
    return;
  }

  if (this->Internals->CurrentAtt)
  {
    delete this->Internals->CurrentAtt;
  }

  int i = view->details().findChild("AttributeTypes");
  if (i < 0)
  {
    return;
  }
  smtk::view::View::Component& comp = view->details().child(i);
  std::string eaName;
  for (std::size_t ci = 0; ci < comp.numberOfChildren(); ++ci)
  {
    smtk::view::View::Component& attComp = comp.child(ci);
    if (attComp.name() != "Att")
    {
      continue;
    }
    std::string optype;
    if (attComp.attribute("Type", optype) && !optype.empty())
    {
      if (optype == "edit core")
      {
        eaName = optype;
        break;
      }
    }
  }
  if (eaName.empty())
  {
    return;
  }

  smtk::attribute::AttributePtr att = this->Internals->CurrentOp->parameters();
  this->Internals->CurrentAtt = this->Internals->createAttUI(att, this->Widget, this);
  if (this->Internals->CurrentAtt)
  {
    QObject::connect(this->Internals->CurrentAtt, &qtAttribute::modified, this,
      &smtkRGGEditCoreView::attributeModified);
    QObject::connect(this->Internals->CurrentAtt, &qtAttribute::itemModified, this,
      &smtkRGGEditCoreView::onAttItemModified);

    this->updateEditCorePanel();
  }
}

void smtkRGGEditCoreView::createWidget()
{
  smtk::view::ViewPtr view = this->getObject();
  if (!view)
  {
    return;
  }

  QVBoxLayout* parentLayout = dynamic_cast<QVBoxLayout*>(this->parentWidget()->layout());

  // Delete any pre-existing widget
  if (this->Widget)
  {
    if (parentLayout)
    {
      parentLayout->removeWidget(this->Widget);
    }
    delete this->Widget;
  }

  // Create a new frame and lay it out
  this->Widget = new QFrame(this->parentWidget());
  QVBoxLayout* layout = new QVBoxLayout(this->Widget);
  layout->setMargin(0);
  this->Widget->setLayout(layout);
  this->Widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

  QWidget* tempWidget = new QWidget(this->parentWidget());
  this->Internals->CurrentWidget = tempWidget;
  this->Internals->setupUi(tempWidget);
  tempWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
  layout->addWidget(tempWidget, 1);

  QObject::connect(this->Internals->launchSchemaPlannerButton, &QPushButton::clicked, this,
    &smtkRGGEditCoreView::launchSchemaPlanner);

  // Show help when the info button is clicked. //
  QObject::connect(
    this->Internals->infoButton, &QPushButton::released, this, &smtkRGGEditCoreView::onInfo);

  QObject::connect(
    this->Internals->applyButton, &QPushButton::released, this, &smtkRGGEditCoreView::apply);

  this->updateAttributeData();
}

void smtkRGGEditCoreView::updateEditCorePanel()
{
  smtk::attribute::AttributePtr att = this->Internals->CurrentAtt->attribute();
  smtk::model::EntityRefArray ents = att->associatedModelEntities<smtk::model::EntityRefArray>();

  auto populateThePanel = [this, &ents]() {
    // Populate the panel
    smtk::model::Model model = ents[0].owningModel();
    smtk::model::EntityRef coreGroup = ents[0];
    Core core = json::parse(coreGroup.stringProperty(Core::propDescription)[0]);

    this->Internals->nameLineEdit->setText(QString::fromStdString(core.name()));

    this->Internals->zOriginLineEdit->setText(QString::number(core.zOrigin()));
    this->Internals->heightLineEdit->setText(QString::number(core.height()));

    this->Internals->ductXThicknessSpinBox->setValue(core.ductThickness().first);
    this->Internals->ductYThicknessSpinBox->setValue(core.ductThickness().second);

    bool isHex = (core.geomType() == Core::GeomType::Hex);
    // Lattice
    this->Internals->latticeYLabel->setHidden(isHex);
    this->Internals->latticeYSpinBox->setHidden(isHex);
    this->Internals->latticeXLabel->setHidden(isHex);
    // Duct thickness
    this->Internals->ductXThicknessLabel->setHidden(isHex);
    this->Internals->ductYThicknessLabel->setHidden(isHex);
    this->Internals->ductYThicknessSpinBox->setHidden(isHex);

    // By default the rect model is 4x4 and hex is 1x1.
    this->Internals->latticeXSpinBox->setValue(core.latticeSize().first);
    this->Internals->latticeYSpinBox->setValue(core.latticeSize().second);
  };

  // Need a valid core
  bool isEnabled(true);
  if ((ents.size() == 0) || (!ents[0].hasStringProperty("rggType")) ||
    (ents[0].stringProperty("rggType")[0] != smtk::session::rgg::Core::typeDescription))
  { // Its type is not rgg core
    isEnabled = false;
    this->Internals->CurrentSMTKCore = smtk::model::EntityRef(); // Invalid the current smtk core
  }
  else
  {
    if (this->Internals->CurrentSMTKCore == ents[0])
    { // If it's the same, do not reset the schema planner
      // since it might surprise the user
      // We still populate the panel in case if the user has imported a core via some operations.
      populateThePanel();
      return;
    }
    this->Internals->CurrentSMTKCore = ents[0]; // Update the current smtk core
  }

  if (this->Internals)
  {
    this->Internals->CurrentWidget->setEnabled(isEnabled);
  }

  if (isEnabled)
  {
    if (!this->Internals->Current2DLattice)
    {
      this->Internals->Current2DLattice = new qtDraw2DLattice(ents[0], this->Internals->coreWidget);
      this->Internals->Current2DLattice->setFrameShape(QFrame::NoFrame);
      // Modify layers/ x and y value would update the schema planner
      QObject::connect(this->Internals->latticeXSpinBox, QOverload<int>::of(&QSpinBox::valueChanged),
        this->Internals->Current2DLattice, &qtDraw2DLattice::setLatticeXorLayers);
      QObject::connect(this->Internals->latticeYSpinBox, QOverload<int>::of(&QSpinBox::valueChanged),
        this->Internals->Current2DLattice, &qtDraw2DLattice::setLatticeY);
      // Reset schema
      QObject::connect(this->Internals->resetSchemaPlannerButton, &QPushButton::clicked,
         this->Internals->Current2DLattice, &qtDraw2DLattice::reset);
      this->Internals->Current2DLattice->setVisible(false);
    }

    // Create/update current rggNucCore
    if (this->Internals->SMTKCoreToCMBCore.contains(ents[0]))
    {
      this->Internals->CurrentCMBCore = this->Internals->SMTKCoreToCMBCore.value(ents[0]);
    }
    else
    {
      rggNucCore* assy = new rggNucCore(ents[0]);
      this->Internals->SMTKCoreToCMBCore[ents[0]] = assy;
      this->Internals->CurrentCMBCore = assy;
    }
    this->Internals->Current2DLattice->setLattice(this->Internals->CurrentCMBCore);
    populateThePanel();
  }
}

void smtkRGGEditCoreView::setInfoToBeDisplayed()
{
  this->m_infoDialog->displayInfo(this->getObject());
}

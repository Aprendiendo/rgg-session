//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/session/rgg/Registrar.h"

#include "smtk/session/rgg/operators/AddMaterial.h"
#include "smtk/session/rgg/operators/CreateModel.h"
#include "smtk/session/rgg/operators/Delete.h"
#include "smtk/session/rgg/operators/EditAssembly.h"
#include "smtk/session/rgg/operators/EditCore.h"
#include "smtk/session/rgg/operators/EditDuct.h"
#include "smtk/session/rgg/operators/EditMaterial.h"
#include "smtk/session/rgg/operators/EditPin.h"
#include "smtk/session/rgg/operators/ExportInp.h"
#include "smtk/session/rgg/operators/Read.h"
#include "smtk/session/rgg/operators/ReadRXFFile.h"
#include "smtk/session/rgg/operators/RemoveMaterial.h"
#include "smtk/session/rgg/operators/Write.h"

#include "smtk/session/rgg/Resource.h"

#include "smtk/operation/RegisterPythonOperations.h"

#include "smtk/operation/groups/CreatorGroup.h"
#include "smtk/operation/groups/ExporterGroup.h"
#include "smtk/operation/groups/ImporterGroup.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

namespace smtk
{
namespace session
{
namespace rgg
{

namespace
{
typedef std::tuple<AddMaterial, CreateModel, Delete,
  EditAssembly, EditCore, EditDuct, EditMaterial, EditPin, ExportInp, Read, ReadRXFFile,
      RemoveMaterial, Write>
  OperationList;
}

void Registrar::registerTo(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->registerResource<smtk::session::rgg::Resource>(read, write);
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations
  operationManager->registerOperations<OperationList>();

  smtk::operation::registerPythonOperations(operationManager, "rggsession.export_to_pyarc");

  smtk::operation::ExporterGroup(operationManager).registerOperation<smtk::session::rgg::Resource>(
    "rggsession.export_to_pyarc.export_to_pyarc");

  smtk::operation::registerPythonOperations(operationManager, "rggsession.generate_mesh");

  smtk::operation::CreatorGroup(operationManager)
    .registerOperation<smtk::session::rgg::Resource, smtk::session::rgg::CreateModel>();

  // smtk::operation::ImporterGroup(operationManager)
  //   .registerOperation<smtk::session::rgg::Resource, smtk::session::rgg::ReadRXFFile>();

   smtk::operation::ReaderGroup(operationManager)
     .registerOperation<smtk::session::rgg::Resource, smtk::session::rgg::Read>();

   smtk::operation::WriterGroup(operationManager)
     .registerOperation<smtk::session::rgg::Resource, smtk::session::rgg::Write>();
}

void Registrar::unregisterFrom(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->unregisterResource<smtk::session::rgg::Resource>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperations<OperationList>();
}
}
}
}

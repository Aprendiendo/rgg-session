#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
import os
import sys
import unittest
import smtk
import smtk.model
import rggsession
import smtk.testing
import rggsession.export_to_pyarc


def readFile(filename, mode="rt"):
    with open(filename, mode) as fin:
        return fin.read()


def deleteFile(filename):
    os.remove(filename)


class ExportToPyARC(smtk.testing.TestCase):

    def setUp(self):
        self.modelFile = os.path.join\
            (smtk.testing.DATA_DIR, 'sampleCore.rxf')
        self.targetFile = os.path.join\
            (smtk.testing.DATA_DIR, 'sampleCoreTest.son')
        self.validationFile = os.path.join \
            (smtk.testing.DATA_DIR, 'sampleCore.son')

    def testReadRXFFileOp(self):
        # create model
        op = smtk.session.rgg.CreateModel.create()
        res = op.operate()
        if res.findInt('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
            raise ImportError
        self.model = res.find('created').objectValue(0)
        # hold onto the resource so it doesn't go out of scope when this
        # operation's result is replaced by the next operation's result
        self.resource = self.model.resource()
        # read rxf file
        op = smtk.session.rgg.ReadRXFFile.create()
        fname = op.parameters().find('filename')
        fname.setValue(self.modelFile)
        op.parameters().associate(self.model)
        res = op.operate()
        if res.findInt('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
            raise RuntimeError
        # export to PyARC
        op = smtk.session.rgg.export_to_pyarc()
        fname = op.parameters().find('filename')
        fname.setValue(self.targetFile)
        op.parameters().associate(self.model)
        res = op.operate()
        if res.findInt('outcome').value(0) != int(smtk.operation.Operation.SUCCEEDED):
            print(op.log().convertToString(True))
            raise RuntimeError

        if readFile(self.validationFile) != readFile(self.targetFile):
            print("The generated file does not match simpleCore.son file!")
            deleteFile(self.targetFile)
            raise RuntimeError
        deleteFile(self.targetFile)


if __name__ == '__main__':
    smtk.testing.process_arguments()
    unittest.main()
